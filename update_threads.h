
typedef struct
{
	int cpuUserPercent;
	int cpuSysPercent;
	int cpuIOWaitPercent;
	int cpuNicePercent;
	
	int usedMemPercent;
	int cachedMemPercent;
	
} SysStatsData;



static pthread_mutex_t stats_mutex = PTHREAD_MUTEX_INITIALIZER;
static SysStatsData system_stats = { 0 };

void *fetchSystemStats(void *arg)
{
	sg_mem_stats *mem_stats;
	sg_cpu_stats *cpu_diff_stats;
	sg_cpu_percents *cpu_percent;
	size_t entries;
	
	while(running)
	{
		pthread_mutex_lock(&stats_mutex);
        	
		cpu_percent = sg_get_cpu_percents(NULL);
		cpu_diff_stats = sg_get_cpu_stats_diff(NULL);
		
		if(cpu_diff_stats && cpu_percent)
		{
		/* user stats */
			char userBuffer[16];
			sprintf(userBuffer,"%0.0f", cpu_diff_stats->user, cpu_percent->user);
			int userInt = atoi(userBuffer);
			
		/* sys/kernel stats */
			char sysBuffer[16];
			sprintf(sysBuffer,"%0.0f", cpu_diff_stats->kernel, cpu_percent->kernel);
			int sysInt = atoi(sysBuffer) + userInt;
			
		/* I/O wait stats */
			char IOwaitBuffer[16];
			sprintf(IOwaitBuffer,"%0.0f", cpu_diff_stats->iowait, cpu_percent->iowait);
			int IOwaitInt = atoi(IOwaitBuffer) + sysInt;
			
		/* nice/priority stats */
			char niceBuffer[16];
			sprintf(niceBuffer,"%0.0f", cpu_diff_stats->nice, cpu_percent->nice);
			int niceInt = atoi(niceBuffer) + IOwaitInt;
			
		/* update widgets that make up CPU usage meter */
			if(displayCPUmeter)
			{
				XtVaSetValues(cpuUserFrame, XmNrightPosition, userInt, NULL);
				XtVaSetValues(cpuSysFrame, XmNrightPosition, sysInt, NULL);
				XtVaSetValues(cpuIOFrame, XmNrightPosition, IOwaitInt, NULL);
				XtVaSetValues(cpuNiceFrame, XmNrightPosition, niceInt, NULL);
			}
			
		/* debug leftovers */
			/*
			printf("User -> %d \n", userInt);
			printf("Sys  -> %d \n", sysInt);
			printf("I/O  -> %d \n", IOwaitInt);
			printf("Nice  -> %d \n", niceInt);
			fflush(stdout);
			*/
		}
		
		
		mem_stats = sg_get_mem_stats(&entries);
		unsigned long long totalMem = mem_stats[0].total;
		if(totalMem > 0)
		{
		/* truly used memory */
			system_stats.usedMemPercent = (int)(((double)mem_stats[0].used / totalMem) * 100);
		
		/* cached memory */
			system_stats.cachedMemPercent = system_stats.usedMemPercent + 
			(int)(((double)mem_stats[0].cache / totalMem) * 100);
			
		/* update widgets that make up RAM usage meter */
			if(displayRAMmeter)
			{
				XtVaSetValues(memUsedFrame, XmNrightPosition, system_stats.usedMemPercent, NULL);
				XtVaSetValues(memCacheFrame, XmNrightPosition, system_stats.cachedMemPercent, NULL);
			}
			
		/* debug leftovers */
			/*
			printf("Memory USED: %d%%\n", system_stats.usedMemPercent);\
			printf("Memory CACHED: %d%%\n", system_stats.cachedMemPercent);
			fflush(stdout);
			*/
		}
		
/*
	Using XmUpdateDisplay() too much seems to freeze the GUI until mouseover.
	Refreshing the topLevel window shell only seems to be most efficient.
*/
		
		Display *display = XtDisplay(topLevel);
		XSync(display, False);
		XFlush(display);

		pthread_mutex_unlock(&stats_mutex);

		/* sleep for 300ms/0.3s */
		usleep(300000);
	}
	
	return NULL;
}

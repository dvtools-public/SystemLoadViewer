
void creatememMeterLabel(Widget parent, char *name, char *color)
{
	memLabel = XtVaCreateManagedWidget("memLabel", xmLabelWidgetClass, parent,
		XmNalignment, XmALIGNMENT_CENTER,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XtVaTypedArg, XmNlabelString, XmRString, name, 4,
		XtVaTypedArg, XmNforeground, XmRString, color, 4,
	NULL);
}

void creatememMeterLabelSpacer(Widget parent, char *name)
{
	memLabel = XtVaCreateManagedWidget("memLabel", xmLabelWidgetClass, parent,
		XmNalignment, XmALIGNMENT_CENTER,
		XmNmarginHeight, 1,
		XmNmarginWidth, 0,
		XtVaTypedArg, XmNlabelString, XmRString, name, 4,
	NULL);
}

void creatememMeterPercBar(Widget parent, int offset, int fill)
{
	Pixel color;
	if(fill == 0) { XtVaGetValues(parent, XmNbackground, &color, NULL); }
	if(fill == 1) { XtVaGetValues(parent, XmNforeground, &color, NULL); }
	
	Widget memPercBar = XtVaCreateManagedWidget("memPercBar", xmFrameWidgetClass, parent,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, offset,
		XmNrightOffset, 0,
		XmNheight, 2,
		XmNshadowThickness, 0,
		XmNbackground, color, /* use color arg passed to function */
	NULL);
}

void installRAMmeter(Widget parent)
{
	Widget memMeterLabelContainer = XtVaCreateManagedWidget("memMeterLabelContainer", xmRowColumnWidgetClass, parent,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		XmNmarginHeight, 0,
		XmNmarginWidth, 4,
		XmNspacing, 1,
		
		#ifdef LABEL_PLACEMENT_DBG
		XtVaTypedArg, XmNbackground, XmRString, "blue", 4,
		#endif
	NULL);
	
	creatememMeterLabelSpacer(memMeterLabelContainer, "RAM Usage: ");
	
	creatememMeterLabel(memMeterLabelContainer, "Used", "#4682b4");
		creatememMeterLabelSpacer(memMeterLabelContainer, "/");
	creatememMeterLabel(memMeterLabelContainer, "Cached", "Orange3");
		creatememMeterLabelSpacer(memMeterLabelContainer, "/");
	creatememMeterLabel(memMeterLabelContainer, "Free", "#3caa71");
	
	
	Pixel parentWidget_fg, parentWidget_bg;
	XtVaGetValues(parent, XmNforeground, &parentWidget_fg, NULL);
	XtVaGetValues(parent, XmNbackground, &parentWidget_bg, NULL);
	
/* mem meter manager form to hold color bars */
	memMeterFrame = XtVaCreateManagedWidget("memMeterFrame", xmFormWidgetClass, parent,
		XmNshadowThickness, 1,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, memMeterLabelContainer,
		XmNtopOffset, 3,
		
		XmNheight, meterHeight,
		XmNfractionBase, 100,
		
		XtVaTypedArg, XmNbackground, XmRString, "#3cb371", 4,
		XmNtopShadowColor, parentWidget_fg,
		XmNbottomShadowColor, parentWidget_fg,
	NULL);
	
	memUsedFrame = XtVaCreateManagedWidget("memUsedFrame", xmFrameWidgetClass, memMeterFrame,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 30,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#4682b4", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#4682b4", 4,
	NULL);
	
	memCacheFrame = XtVaCreateManagedWidget("memCacheFrame", xmFrameWidgetClass, memMeterFrame,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 50,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "Orange3", 4,
		XtVaTypedArg, XmNbackground, XmRString, "Orange3", 4,
	NULL);

	
	
	Widget memMeterPercBarForm = XtVaCreateManagedWidget("memMeterPercBarForm", xmFormWidgetClass, parent,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, memMeterFrame,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		
		XmNfractionBase, 100,
		XmNshadowThickness, 0,
	NULL);
	
	
	creatememMeterPercBar(memMeterPercBarForm, 10, 1);
		creatememMeterPercBar(memMeterPercBarForm, 11, 0);
	creatememMeterPercBar(memMeterPercBarForm, 20, 1);
		creatememMeterPercBar(memMeterPercBarForm, 21, 0);
	creatememMeterPercBar(memMeterPercBarForm, 30, 1);
		creatememMeterPercBar(memMeterPercBarForm, 31, 0);
	creatememMeterPercBar(memMeterPercBarForm, 40, 1);
		creatememMeterPercBar(memMeterPercBarForm, 41, 0);
	creatememMeterPercBar(memMeterPercBarForm, 50, 1);
		creatememMeterPercBar(memMeterPercBarForm, 51, 0);
	creatememMeterPercBar(memMeterPercBarForm, 60, 1);
		creatememMeterPercBar(memMeterPercBarForm, 61, 0);
	creatememMeterPercBar(memMeterPercBarForm, 70, 1);
		creatememMeterPercBar(memMeterPercBarForm, 71, 0);
	creatememMeterPercBar(memMeterPercBarForm, 80, 1);
		creatememMeterPercBar(memMeterPercBarForm, 81, 0);
	creatememMeterPercBar(memMeterPercBarForm, 90, 1);
		creatememMeterPercBar(memMeterPercBarForm, 91, 0);
	creatememMeterPercBar(memMeterPercBarForm, 100, 1);
}

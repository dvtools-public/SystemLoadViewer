/*
	BSD Zero Clause License (0BSD)

	Permission to use, copy, modify, and/or distribute this
	software for any purpose with or without fee is hereby
	granted.

	THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS
	ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
	INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
	WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
	TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
	THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h> /* system() */
#include <string.h>
#include <unistd.h> /* usleep(), gethostname() */
#include <pthread.h> /* POSIX multi-threading */
#include <signal.h>
#include <stdbool.h> /* gimme boolean type */

#include <statgrab.h> /* gets CPU usage stats */

#include <Xm/Xm.h> /* motif core */
#include <Xm/CascadeB.h> /* cascading menus in menuBar */
#include <Xm/PushB.h>
#include <Xm/RowColumn.h> /* radio boxes and similar placement behavior */
#include <Xm/Form.h>
#include <Xm/Label.h>
#include <Xm/ToggleB.h> /* radio and toggle buttons */
#include <Xm/Frame.h>
#include <Xm/Separator.h>
#include <Xm/Protocols.h>

#include <X11/Xlib.h>
#include <X11/Intrinsic.h> /* XInitThreads() */
#include <X11/Shell.h>
#include <X11/Xatom.h> /* get motif WM properties */
#include <X11/extensions/Xinerama.h> /* window centering */
#include <X11/xpm.h> /* pixmaps */


/* global variables for libstatgrab */
sg_cpu_percents *cpu_percent = NULL;
sg_cpu_stats *cpu_diff_stats = NULL;
sg_mem_stats *mem_stats = NULL;


/* define some posix threads */
static pthread_t gui_thread;
static pthread_t stats_thread;
static volatile bool running = 1;

typedef struct
{
	int argc;
	char **argv;
	
} MotifThreadArgs;


/* random global vars for the GUI */
int meterHeight = 30;
int displayHostnameInTitle = 0;
int centeredWindowFirstRun = 0;

/* CPU */
int displayCPUmeter = 0;
int userInt = 0;
int sysInt = 0;
int IOwaitInt = 0;
int niceInt = 0;

/* RAM */
int displayRAMmeter = 0;
int usedInt = 0;
int cacheInt = 0;


/* set up global access to Xt display and application contexts */
Display *display;
XtAppContext app;

/* declare global widgets for... */
/* motif_gui.h */
Widget topLevel, masterContainer, popupMenu, popupAbove;

/* cpu_meter.h */
Widget cpuLabel, cpuUserFrame, cpuSysFrame, cpuIOFrame, cpuNiceFrame;

/* mem_meter.h */
Widget memMeterFrame, memLabel, memUsedFrame, memCacheFrame;


/* optional debugging switches */
/*
#define SHOW_CALLBACK_MSGS_DBG
#define LABEL_PLACEMENT_DBG
*/

#include "xresources.h" /* fallback X11 resources */
#include "callbacks.h" /* callbacks for widgets */

#include "cpu_meter.h"
#include "mem_meter.h"
#include "update_threads.h"
#include "motif_gui.h"

int main(int argc, char *argv[])
{
/* handle command line flags */
	interceptCLIargs(argc, argv);
	
/* enable multi-threading support for X11 - XSync(), XFlush(), XtAppMainLoop(), etc. from other threads */
	XInitThreads();

/* pthread to filter out stderr warnings from inflexible widgets */
	filterToolkitMsgs();
	
/* pthread to create the main graphical window */
	createUserInterface(argc, argv);
	
/* 0.5 second delay before trying any widget updates */
	usleep(500000);
	
/* initialize libstatgrab core */	
	if(sg_init(0) != 0)
	{
		fprintf(stderr, "\ndvslv: main(): Failed to initialize libstatgrab!\n\n");
		fflush(stderr);
		exit(1);
	}
	
/* take initial libstatgrab snapshot */
	sg_snapshot();
	
/* pthread to collect system stats */
	if(pthread_create(&stats_thread, NULL, fetchSystemStats, NULL) != 0)
	{
		fprintf(stderr, "\ndvslv: main(): Failed to spawn stats collection thread!\n\n");
		fflush(stderr);
		sg_shutdown();
		exit(1);
	}
	
/* a fruitloop a day keeps the fork() away!  */
	while(1)
	{
		sleep(86400);
		NULL;
	}
	
	return 0;
}
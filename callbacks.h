void printFancyHeader(char *functionName)
{
	const char *programName = "\n \033[1;4;93mSystem Load Viewer\033[m";
	const char *shortArrow = "\033[0;96m->\033[m";
	fprintf(stdout, "%s %s %s", programName, shortArrow, functionName);
	fflush(stdout);
}



void printFancyMessage(char *messageString)
{
	const char *longArrow = "\033[0;96m  -->\033[m";
	fprintf(stdout, "%s %s", longArrow, messageString);
	fflush(stdout);
}



void instantQuit()
{
	#ifdef SHOW_CALLBACK_MSGS_DBG
	printFancyHeader("\033[1;92m instantQuit \033[m\n\n");
	printFancyMessage("User has chosen to quit.\n\n");
	#endif
	
/* shut down stats collection thread */
	running = 0;
	sg_shutdown();
	pthread_join(stats_thread, NULL);
	
	
	exit(0);
}



void interceptCLIargs(int argc, char *argv[])
{
	if(argc <= 1) /* no command line flags... */
	{	
		displayCPUmeter = 1;
		displayRAMmeter = 1;
		return;
	}
	
	for(int i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "-help") == 0  || strcmp(argv[i], "--help") == 0)
		{
			printf("\033[7m%s\033[0m\n", "\n DeskView System Load Viewer v1.2 ");
			
			printf("\n  * Only the CPU meter is shown by default.\n");
			
			printf("\n  * The popup menu can be accessed by right");
			printf("\n    clicking in the main window.\n");
			
			printf("\n  * Selecting \'Top of Stack\' will tell the");
			printf("\n    program to spawn another thread to");
			printf("\n    continuously raise the window.\n");
			
			printf("\033[7m%s\033[0m\n", "\n Options: \n");
			printf("  -help       Print this exact help text.\n");
			printf("  -host       Set hostname as window title.\n");
			printf("  -mem        Show memory meter.\n");
			
			printf("\033[7m%s\033[0m\n", "\n Example Usage: \n");
			printf("  ~$ ./dvslv -host -mem &\n\n");
			
			fflush(stdout);
			exit(0);
		}
		
/* track feature flags */
		int hasFeatureFlag = 0;
		
		if(strcmp(argv[i], "-host") == 0)
		{
			displayHostnameInTitle = 1;
			continue;
		}
        
		if(strcmp(argv[i], "-cpu") == 0)
		{
			displayCPUmeter = 1;
			hasFeatureFlag = 1;
			continue;
		}
		
		if(strcmp(argv[i], "-mem") == 0)
		{
			displayRAMmeter = 1;
			hasFeatureFlag = 1;
			continue;
		}
		
/* if -host is the only flag provided, show default meters */
/* (currently very buggy and prevents GUI updates ) */
		if(displayHostnameInTitle && !hasFeatureFlag)
		{
			displayCPUmeter = 1;
			displayRAMmeter = 1;
		}
	}
}



/* filter out stderrs from inflexible Motif widgets */
static int pipefd[2];
static int orig_stderr;
static char buffer[4096];
static pthread_t filter_thread;
static volatile int keep_running = 1;

void* filter_errors(void* arg)
{
	ssize_t n;
	while(keep_running)
	{
		n = read(pipefd[0], buffer, sizeof(buffer));
		if(n > 0)
		{
			if(strstr(buffer, "XmForm") == NULL)
			{
				write(orig_stderr, buffer, n);
			}
		}
	}
	
	return NULL;
}



int filterToolkitMsgs(void)
{
/* saves original stderr */
	orig_stderr = dup(STDERR_FILENO);
	
/* intercept stderr */
	if(pipe(pipefd) == -1)
	{
		perror("pipe");
		return -1;
	}
	
/* redirect into pipe */
	dup2(pipefd[1], STDERR_FILENO);
	
/* spawn filtering thread */
	if(pthread_create(&filter_thread, NULL, filter_errors, NULL) != 0)
	{
		perror("pthread_create");
		close(pipefd[0]);
		close(pipefd[1]);
		return -1;
	}
	
	return 0;
}



void rightClickCallback(Widget widget, XtPointer clientData, XEvent* event, Boolean* continueProcessing)
{
	if (event->type == ButtonPress && event->xbutton.button == Button3) /* if right click was intercepted... */
	{
		Widget popupMenu = (Widget)clientData; /* make sure widget data is accessible */
		XmMenuPosition(popupMenu, (XButtonPressedEvent*)event); /* post menu under mouse pointer */
		XtManageChild(popupMenu);
		*continueProcessing = False;
	}
}



void iconifyWindow()
{
	#ifdef SHOW_CALLBACK_MSGS_DBG
	printFancyHeader("\033[1;92m iconifyWindow \033[m\n\n");
	printFancyMessage("Attempting to iconify/minimize window...\n\n");
	#endif
	
	XtVaSetValues(topLevel,
		XmNiconic, 1, /* tells topLevel shell to minimize */
	NULL);
}



pthread_t keepAboveThread;

int keepAbove = 0;
void *keepWindowAbove()
{
	while(keepAbove) /* while keepAbove is 1 (true)... */
	{
		/* get Xt display and window */
		Display *display = XtDisplay(topLevel);
		Window window = XtWindow(topLevel);
	
		/* bring window to top of stack */
		XRaiseWindow(display, window);
		
		/* force whole GUI to update immediately */
		XFlush(display);
		
		/* sets update interval */
		unsigned int microSeconds = 1000000; /* 1.0 sec */
		usleep(microSeconds);
	}
	
	#ifdef SHOW_CALLBACK_MSGS_DBG
	printFancyHeader("\033[1;92m keepWindowAbove \033[m\n\n");
	printFancyMessage("Removed pthread \'keepAboveThread\'\n");
	printFancyMessage("Window should now stack normally.\n\n");
	#endif
}



void toggleAlwaysOnTop()
{
	if(!keepAbove) /* if keepAbove other windows is false... */
	{
		/* toggle thread flag */
		keepAbove = 1;
		/* spawn another POSIX thread */
		pthread_create(&keepAboveThread, NULL, keepWindowAbove, NULL);
		
		/* set label text on menu entry */
		XtVaSetValues(popupAbove, XtVaTypedArg, XmNlabelString, XmRString, "Stack Normally", 4, NULL);
		
		#ifdef SHOW_CALLBACK_MSGS_DBG
		printFancyHeader("\033[1;92m toggleAlwaysOnTop \033[m\n\n");
		printFancyMessage("Spawning pthread \'keepAboveThread\'\n");
		printFancyMessage("Attempting to continuously raise window...\n\n");
		#endif
	}
	
	else if(keepAbove) /* else if true... */
	{
		keepAbove = 0;
		pthread_join(keepAboveThread, NULL); /* removes thread */
		
		XtVaSetValues(popupAbove, XtVaTypedArg, XmNlabelString, XmRString, "Top of Stack", 4, NULL);
	}
}



int *getFrameExtents(Widget widget)
{
/* this array is for the border sizes in pixels */
	static int extents[4] = {0, 0, 0, 0};

/* grab display and window associated with input widget */
	Display *display = XtDisplay(widget);
	Window window = XtWindow(widget);

/* sets the XAtom we're looking for */
	Atom property = XInternAtom(display, "_NET_FRAME_EXTENTS", True);

/* return nothing and exit function if prop not found */
	if(property == None)
	{
		printf("\n Could not find _NET_FRAME_EXTENTS property. \n");
		fflush(stdout);
		
		return NULL;
	}

/* if property was found, fetch values */
	Atom actualType;
	int actualFormat;
	unsigned long itemCount, bytesAfter;
	unsigned char *propReturn = NULL;
	
	if(XGetWindowProperty(display, window, property, 0, 4, False, XA_CARDINAL,
	&actualType, &actualFormat, &itemCount, &bytesAfter, &propReturn) == Success)
	{
		if(actualType == XA_CARDINAL && actualFormat == 32 && itemCount == 4)
		{
			long *extentsLong = (long *)propReturn;
			extents[0] = (int)extentsLong[0]; /* left */
			extents[1] = (int)extentsLong[1]; /* right */
			extents[2] = (int)extentsLong[2]; /* top - includes title bar */
			extents[3] = (int)extentsLong[3]; /* bottom */
		}
		
		/* clean up */
		XFree(propReturn);
	}

/* send out results */
	return extents;
}



void centerWindow(Widget widget, XtPointer client_data, XEvent *event, Boolean *cont)
{
/* 
	This is needed since iconify/restore operations from
	the window manager will trigger a MapNotify event all
	over again. This will result in super annoyed users
	who expect the window to be where they left it.
*/
	if(centeredWindowFirstRun)
	{
		return; /* do nothing if window was already centered at startup */
	}
	
	
/* if top shell has been fully mapped... */
	if(event->type == MapNotify)
	{
		centeredWindowFirstRun = 1;
		
/* get _NET_FRAME_EXTENTS for window border and title bar */
		int borderTop, borderLeft, borderRight, borderBottom;
		int *extents = getFrameExtents(widget);
		if(extents != NULL)
		{
			borderTop = extents[2];
			borderLeft = extents[0];
			borderRight = extents[1];
			borderBottom = extents[3];
		}
		
/* grab display and window associated with input widget */
		Display *display = XtDisplay(widget);
		Window window = XtWindow(widget);
		
/* grab screen count from xinerama */
		int screen_count;
		XineramaScreenInfo *screen_info = XineramaQueryScreens(display, &screen_count);
		
		if(!screen_info || screen_count == 0) /* if no screens... */
		{
			fprintf(stderr, "\n Could not retrieve Xinerama screen info. \n");
			fflush(stderr);
			
			XFree(screen_info);
			return;
		}
		
/* grab root window from display (topLevel) */
		RootWindow(display, screen_info[0].screen_number);
		
/* grab dimensions of input widget */
		Dimension winWidth, winHeight;
		XtVaGetValues(widget,
			XmNwidth, &winWidth,
			XmNheight, &winHeight,
		NULL);
		
/* calculate new window position */
		int newX = (screen_info[0].width / 2) - winWidth / 2 - borderLeft;
		int newY = (screen_info[0].height / 2) - winHeight / 2 - (borderBottom * 2);
		
/* move window to new position */
		XMoveWindow(display, window, newX, newY);
		
		/* clean up */
		XFree(screen_info);
	}
}


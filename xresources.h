static String fb_xres[] = 
{	
/* optional font settings */
	"*renderTable: xft",
	"*fontType: FONT_IS_XFT",
	"*fontName: Roboto",
	"*fontSize: 11",
	"*autohint: 1",
	"*lcdfilter: lcddefault",
	"*hintstyle: hintslight",
	"*hinting: True",
	"*antialias: 1",
	"*rgba: rgb",
	"*dpi: 96",
	
	"*cpuLabel*fontStyle: Bold Italic",
	"*memLabel*fontStyle: Bold Italic",
	"*diskLabel*fontStyle: Bold Italic",
	"*netLabel*fontStyle: Bold Italic",
	"*gfxLabel*fontStyle: Bold Italic",

/* optional color settings * /
	"*background: black",
	"*foreground: white",
*/
		
	NULL,
};




### System Load Viewer

This is a resource monitor for X11 on Linux/Unix, inspired by [gr_osview](screenshots/gr_osview.png) from IRIX.
It uses Motif for the GUI components and libstatgrab on the backend. Only tested on Devuan Linux x86_64.



![screenshot](screenshots/dvslv_sample.png)


![screenshot](screenshots/dvslv_sample2.png)

#### Extra Features

The popup menu can be accessed by right clicking in the main window.

- Supports -host argument to add hostname to window title.
- Ctrl + S - Keep window at top of stack above all others.
- Ctrl + I - Iconify/minimize the window.
- Ctrl + Q - Quit.

#### Known Defects

Passing only the -host flag through the command line will prevent the GUI from updating. If you use the -host flag you must also specify at least one meter type.

#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXpm
- libXinerama
- libstatgrab

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvslv -host -cpu -mem &
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```



#### License

This software is distributed free of charge under the BSD Zero Clause license.
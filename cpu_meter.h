
void createCPUMeterLabel(Widget parent, char *name, char *color)
{
	cpuLabel = XtVaCreateManagedWidget("cpuLabel", xmLabelWidgetClass, parent,
		XmNalignment, XmALIGNMENT_CENTER,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XtVaTypedArg, XmNlabelString, XmRString, name, 4,
		XtVaTypedArg, XmNforeground, XmRString, color, 4,
	NULL);
}

void createCPUMeterLabelSpacer(Widget parent, char *name)
{
	cpuLabel = XtVaCreateManagedWidget("cpuLabel", xmLabelWidgetClass, parent,
		XmNalignment, XmALIGNMENT_CENTER,
		XmNmarginHeight, 1,
		XmNmarginWidth, 0,
		XtVaTypedArg, XmNlabelString, XmRString, name, 4,
	NULL);
}

void createCPUMeterPercBar(Widget parent, int offset, int fill)
{
	Pixel color;
	if(fill == 0) { XtVaGetValues(parent, XmNbackground, &color, NULL); }
	if(fill == 1) { XtVaGetValues(parent, XmNforeground, &color, NULL); }
	
	Widget cpuPercBar = XtVaCreateManagedWidget("cpuPercBar", xmFrameWidgetClass, parent,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, offset,
		XmNrightOffset, 0,
		XmNheight, 2,
		XmNshadowThickness, 0,
		XmNbackground, color, /* use color arg passed to function */
	NULL);
}

void installCPUmeter(Widget parent)
{
	Widget cpuMeterLabelContainer = XtVaCreateManagedWidget("cpuMeterLabelContainer", xmRowColumnWidgetClass, parent,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNorientation, XmHORIZONTAL,
		XmNpacking, XmPACK_TIGHT,
		XmNmarginHeight, 0,
		XmNmarginWidth, 4,
		XmNspacing, 1,
		
		#ifdef LABEL_PLACEMENT_DBG
		XtVaTypedArg, XmNbackground, XmRString, "blue", 4,
		#endif
	NULL);
	
	createCPUMeterLabelSpacer(cpuMeterLabelContainer, "CPU Usage: ");
	
	createCPUMeterLabel(cpuMeterLabelContainer, "User", "#cd5c5c");
		createCPUMeterLabelSpacer(cpuMeterLabelContainer, "/");
	createCPUMeterLabel(cpuMeterLabelContainer, "System", "#4682b4");
		createCPUMeterLabelSpacer(cpuMeterLabelContainer, "/");
	createCPUMeterLabel(cpuMeterLabelContainer, "I/O", "#daa520");
		createCPUMeterLabelSpacer(cpuMeterLabelContainer, "/");
	createCPUMeterLabel(cpuMeterLabelContainer, "Nice", "#ff7f50");
		createCPUMeterLabelSpacer(cpuMeterLabelContainer, "/");
	createCPUMeterLabel(cpuMeterLabelContainer, "Idle", "#3caa71");
	
	
	Pixel parentWidget_fg, parentWidget_bg;
	XtVaGetValues(parent, XmNforeground, &parentWidget_fg, NULL);
	XtVaGetValues(parent, XmNbackground, &parentWidget_bg, NULL);
	
/* cpu meter manager form to hold color bars */
	Widget cpuMeterFrame = XtVaCreateManagedWidget("cpuMeterFrame", xmFormWidgetClass, parent,
		XmNshadowThickness, 1,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, cpuMeterLabelContainer,
		XmNtopOffset, 3,
		
		XmNheight, 32,
		XmNfractionBase, 100,
		
		XtVaTypedArg, XmNbackground, XmRString, "#3cb371", 4,
		XmNtopShadowColor, parentWidget_fg,
		XmNbottomShadowColor, parentWidget_fg,
	NULL);
	
	cpuUserFrame = XtVaCreateManagedWidget("cpuUserFrame", xmFrameWidgetClass, cpuMeterFrame,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 30,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#cd5c5c", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#cd5c5c", 4,
	NULL);
	
	cpuSysFrame = XtVaCreateManagedWidget("cpuSysFrame", xmFrameWidgetClass, cpuMeterFrame,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 50,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#4682b4", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#4682b4", 4,
	NULL);

	cpuIOFrame = XtVaCreateManagedWidget("cpuIOFrame", xmFrameWidgetClass, cpuMeterFrame,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 50,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#daa520", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#daa520", 4,
	NULL);
	
	cpuNiceFrame = XtVaCreateManagedWidget("cpuNiceFrame", xmFrameWidgetClass, cpuMeterFrame,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 50,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#ff7f50", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#ff7f50", 4,
	NULL);
	
	Widget cpuMeterPercBarForm = XtVaCreateManagedWidget("cpuMeterPercBarForm", xmFormWidgetClass, parent,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, cpuMeterFrame,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		
		XmNfractionBase, 100,
		XmNshadowThickness, 0,
	NULL);
	
	
	createCPUMeterPercBar(cpuMeterPercBarForm, 10, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 11, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 20, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 21, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 30, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 31, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 40, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 41, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 50, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 51, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 60, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 61, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 70, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 71, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 80, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 81, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 90, 1);
		createCPUMeterPercBar(cpuMeterPercBarForm, 91, 0);
	createCPUMeterPercBar(cpuMeterPercBarForm, 100, 1);
}



void *spawnMotifGUI(void *arg)
{
	MotifThreadArgs *args = (MotifThreadArgs *)arg;
	int argc = args->argc;
	char **argv = args->argv;
	
/* init X11 toolkit and application context */
	XtToolkitInitialize();
	app = XtCreateApplicationContext();
	
	/* setup localization */
	XtSetLanguageProc(NULL, NULL, NULL);
	
	/* open up X11 display connection */
	display = XtOpenDisplay(app, NULL, argv[0], "App", NULL, 0, &argc, argv);
	
	if(!display) /* error if no display */
	{
		fprintf(stderr, "Could not open XtDisplay\n");
		pthread_exit(NULL);
	}
	
	/* create top level shell */
	topLevel = XtVaAppInitialize(&app, "dvslv", NULL, 0, &argc, argv, fb_xres, NULL);
	
	/* set some initial resources for topLevel*/
	XtVaSetValues(topLevel,
		XmNtitle, "System Load Viewer",
		XmNiconName, " System Load Viewer ",
		XmNminWidth, 420,
		XmNwidth, 420,
	NULL);
	
	if(displayHostnameInTitle)
	{
		char hostname[1024];
		gethostname(hostname, sizeof(hostname));
		char combinedTitle[sizeof(hostname)];
		snprintf(combinedTitle, sizeof(hostname), "System Load Viewer - %s", hostname);
		
		XtVaSetValues(topLevel,
			XmNtitle, combinedTitle,
		NULL);
	}
	
/* master container widget to hold all meters and labels */
	masterContainer = XtVaCreateManagedWidget("masterContainer", xmRowColumnWidgetClass, topLevel,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNspacing, 0,
		XmNmarginWidth, 8,
		XmNmarginHeight, 4,
	NULL);
	
/* get color from XmForm */
	Pixel masterContainer_bg, masterContainer_fg;
	XtVaGetValues(masterContainer, XmNbackground, &masterContainer_bg, NULL);
	XtVaGetValues(masterContainer, XmNforeground, &masterContainer_fg, NULL);
	
/* create right click popup */
	Widget rightClickPopup = XmCreatePopupMenu(masterContainer, "rightClickPopup", NULL, 0);
	XtVaSetValues(rightClickPopup,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, masterContainer_fg,
		XmNbottomShadowColor, masterContainer_fg,
	NULL);

/* toggle window always on top */
	popupAbove = XtVaCreateManagedWidget("popupAbove", xmPushButtonWidgetClass, rightClickPopup,
		XmNmnemonic, 'S',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, masterContainer_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>S", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + S", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Top of Stack", 4,
	NULL);
	XtAddCallback(popupAbove, XmNactivateCallback, toggleAlwaysOnTop, NULL);
	
/* iconify window */
	Widget popupIconify = XtVaCreateManagedWidget("popupIconify", xmPushButtonWidgetClass, rightClickPopup,
		XmNmnemonic, 'I',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, masterContainer_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>I", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + I", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Iconify", 4,
	NULL);
	XtAddCallback(popupIconify, XmNactivateCallback, iconifyWindow, NULL);
	
	Widget popupSep = XtVaCreateManagedWidget("popupSep", xmSeparatorWidgetClass, rightClickPopup, NULL);
	
/* quit immediately */
	Widget popupQuit = XtVaCreateManagedWidget("popupQuit", xmPushButtonWidgetClass, rightClickPopup,
		XmNmnemonic, 'Q',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, masterContainer_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>Q", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + Q", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Quit", 4,
	NULL);
	XtAddCallback(popupQuit, XmNactivateCallback, instantQuit, NULL);
	
/* event handler for the right click */
	XtAddEventHandler(masterContainer, ButtonPressMask, False, rightClickCallback, rightClickPopup);
	
	
/* create resource meters and modify other widgets based on command line arguments */
	if(displayCPUmeter)
	{
		installCPUmeter(masterContainer);
	}
	
	if(displayRAMmeter)
	{
		if(displayCPUmeter && displayRAMmeter) /* no padding req. if only mem meter is shown */
		{
			Widget memSep = XtVaCreateManagedWidget("memSep", xmSeparatorWidgetClass, masterContainer,
				XmNshadowThickness, 0,
				XmNheight, 3,
			NULL);
		}
		
		/* create meter for memory usage */
		installRAMmeter(masterContainer);
	}
	
	if(!displayCPUmeter && !displayRAMmeter)
	{
		installCPUmeter(masterContainer);
		
		/* add invisible separator as a spacer to pad text */
		Widget memSep = XtVaCreateManagedWidget("memSep", xmSeparatorWidgetClass, masterContainer,
			XmNshadowThickness, 0,
			XmNheight, 3,
		NULL);
		
		/* create meter for memory usage */
		installRAMmeter(masterContainer);
	}


/* close top level */
	XtRealizeWidget(topLevel);
	
/* set minimum window height based on height of widgets inside masterContainer */
	Dimension masterContainer_h;
	XtVaGetValues(masterContainer, XmNheight, &masterContainer_h, NULL);
	
	int adjusted_h = masterContainer_h + 5;
	XtVaSetValues(topLevel,
		XmNminHeight, adjusted_h,
		XmNheight, adjusted_h,
	NULL);
	
/* center window on first monitor after the main window is fully created */
	XtAddEventHandler(topLevel, StructureNotifyMask, False, centerWindow, NULL);
	
/* enter main GUI processing loop */
	XtAppMainLoop(app);
	
	return NULL;
}



void createUserInterface(int argc, char *argv[])
{
/* Allocate and initialize thread arguments */
	MotifThreadArgs *args = (MotifThreadArgs *)malloc(sizeof(MotifThreadArgs));
	args->argc = argc;
	args->argv = argv;
	
/* spawn thread to run main Motif GUI loop */
	if(pthread_create(&gui_thread, NULL, spawnMotifGUI, (void *)args) != 0)
	{
		perror("createUserInterface: pthread_create could not create Motif GUI thread.");
		free(args);
		exit(1);
	}
}
